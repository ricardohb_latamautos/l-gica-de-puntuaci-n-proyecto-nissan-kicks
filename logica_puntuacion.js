var q_ = [	'A veces llego a…',
			'Mis amigos dicen que soy…',
			'Deseo más que todo…',
			'Mi miedo más grande es…'];
			
var a_ = [	[	{a:'… buscar mis propios caminos',e:5.12,r:2.22,b:-1.21,c:-2.22},
				{a:'… ser atrevido y retar a otros',e:4.23,r:2.21,b:-2.43,c:2.12},
				{a:'… a aburrirme con la monotonía',e:-1.12,r:-3.21,b:4.21,c:2.21},
				{a:'… estar fuera de lo convencional',e:-2.31,r:-2.32,b:1.21,c:8.53}],
			[	{a:'… auténtico y espontáneo',e:7.13,r:2.21,b:-1.23,c:-3.24},
				{a:'… radicalmente libre',e:-1.23,r:5.21,b:0.67,c:-3.46},
				{a:'… relajado y sin preocupaciones',e:-1.43,r:2.21,b:3.23,c:1.13},
				{a:'… imaginativo y creativo',e:2.44,r:-3.11,b:1,c:7.21}],
			[	{a:'… la libertad de descubrir quién soy',e:4.21,r:1.41,b:0.12,c:-1.32},
				{a:'… gritar lo que me gusta',e:3.34,r:8.21,b:-1.32,c:0.43},
				{a:'… no tomarme las cosas tan en serio',e:-1.43,r:3.32,b:9.76,c:2.34},
				{a:'… crear cosas que trascienden',e:-2.43,r:-4.89,b:-1.43,c:4.86}],
			[	{a:'… ser predecible ',e:6.32,r:4.21,b:-2.32,c:-3.31},
				{a:'… no ser decisivo',e:-3.42,r:4.11,b:1.54,c:2.65},
				{a:'… abandonar mi originalidad',e:0.32,r:3.67,b:7.87,c:2.43},
				{a:'… perder mi visión única del mundo',e:-1.67,r:-4.66,b:-1.78,c:5.54}]];

var resultado_final,c = 0;

$(document).ready(init);

function init()
{
	setQ();
}

function setQ()
{
	var c = q_.shift();
	$('.quest').html('Pregunta: ' + c);
	
	$('.answ').html('');
	var i = 0,
		a = a_.shift();
	
	do
	{
		$('.answ').append('<li data-e="' + a[i].e + '" data-r="' + a[i].r + '" data-b="' + a[i].b + '" data-c="' + a[i].c + '">' + a[i].a + '</li>');
	}
	while(++i < a.length)
	
	$('.answ li').click(answHandler);
}

function answHandler()
{
	var e = parseFloat($(this).attr('data-e')),
		r = parseFloat($(this).attr('data-r')),
		b = parseFloat($(this).attr('data-b')),
		c = parseFloat($(this).attr('data-c'));
		
	var ce = parseFloat($('.sum .se').find('span').text()),
		cr = parseFloat($('.sum .sr').find('span').text()),
		cb = parseFloat($('.sum .sb').find('span').text()),
		cc = parseFloat($('.sum .sc').find('span').text());
	
	$('.sum .se').find('span').text((e + ce).round(3));
	$('.sum .sr').find('span').text((r + cr).round(3));
	$('.sum .sb').find('span').text((b + cb).round(3));
	$('.sum .sc').find('span').text((c + cc).round(3));
	
	if(q_.length)
	{
		setQ();
	}
	else
	{
		finalize();
		
		processResult({'e':(e + ce).round(3),'r':(r + cr).round(3),'b':(b + cb).round(3),'c':(c + cc).round(3)});
	}
}

function finalize()
{
	$('.quest').html('Resultados: ');
	$('.answ').remove();
}

function processResult(o)
{
	var r,n = 0;
	
	if(o.e > n){n = o.e;r = 'explorador';}
	if(o.r > n){n = o.r;r = 'rebelde';}
	if(o.b > n){n = o.b;r = 'bufon';}
	if(o.c > n){n = o.c;r = 'creador';}
	
	resultado_final = r;
	
	console.log(resultado_final);
}

Number.prototype.round = function(p)
{
	p = p || 10;
	return parseFloat(this.toFixed(p));
};