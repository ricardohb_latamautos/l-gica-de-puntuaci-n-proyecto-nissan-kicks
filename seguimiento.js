$(document).ready(fl_init);

var country;

function fl_init()
{
	fl_processData();
}

function fl_processData()
{
	country = fl_uv()['c'];
	
	if(typeof country == 'undefined')
	{
		fl_gc();
	}
	else
	{
		fl_set();
	}
}

function fl_gc()
{
	$.get("https://ipinfo.io",fl_pc,'jsonp');
}


function fl_pc(r)
{
	console.log('[ipinfo.io retrieved: ' + r.ip + ' - ' + r.country + ']');
	
	country = r.country;
	
	fl_set();
}

function fl_uv()
{
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value){vars[key] = value;});
	return vars;
}

function fl_set()
{
	console.log(country);
	
	$('.cc').append(country);
}